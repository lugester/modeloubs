create database ubs;
drop database ubs;
use ubs;

create table unidade(
	cod_unidade int auto_increment not null,
    nome varchar(50),
    endereco varchar(70),
    id_dir int,
    constraint PK_UNI primary key(cod_unidade)
	
);

-- constraint FK_DIR foreign key unidade(id_dir) references direcao(id_dir)
 alter table unidade add constraint FK_DIR foreign key unidade(id_dir) references funcionario(cod_func);


create table if not exists paciente(
    num_sus int not null,
    nome varchar(45) not null,
    rg varchar(9) not null,
	cpf varchar(11) not null,
    data_nasc date not null,
    endereco varchar(60) not null,
    telefone varchar(13),
    email varchar(40),
    constraint PK_PACIENTE primary key(num_sus)
);


create table if not exists cargo(
	cod_cargo int,
    nome varchar(70) not null,
    
    constraint PK_CARGO primary key(cod_cargo)
);

create table if not exists funcionario(
	cod_func int,
    nome varchar(45) not null,
    data_nasc date not null,
    cod_cargo int not null,
    cpf varchar(11) not null,	
    endereco varchar(100) not null,
    permissao varchar(1) not null CHECK(permissao = 'A' OR permissao = 'R' OR permissao = 'D'),
    senha varchar(20) not null,
    unidade int not null,
    telefone varchar(13) not null,
    email varchar(40) not null,
    constraint PK_FUNC primary key(cod_func),
    constraint FK_CARGO foreign key funcionario(cod_cargo) references cargo(cod_cargo),
    constraint FK_UNIDADE foreign key funcionario(unidade) references unidade(cod_unidade)
);


alter table funcionario add column telefone varchar(11) not null;
-- alter table funcionario add column 
select * from funcionario;

create table if not exists atendimento(
	id int auto_increment, 
    id_pac int not null,
    cod_func int not null,
    data_agendamento datetime not null,
    dataMarcada timestamp not null,
    status_atend varchar(1) not null CHECK(status_atend = "R" OR status_atend = "N"), -- realizado ou não realizado
    observacao varchar(120),
	
    constraint PK_ATEND primary key(id),
    constraint FK_PACIENTE foreign key atendimento(id_pac) references paciente(num_sus),
    constraint FK_MEDICO foreign key atendimento(cod_func) references funcionario(cod_func)
);

create table if not exists escala(
	cod_escala int not null auto_increment,
	cod_func int not null,
    segunda datetime,
    terca datetime,
    quarta datetime,
    quinta datetime,
    sexta datetime,
    
    constraint PK_ESCALA primary key(cod_escala),
    constraint FK_FUNC foreign key escala(cod_func) references funcionario(cod_func)
);

create table if not exists fornecedor(
	cod_fornecedor int,
    nome varchar(60),

    constraint PK_FORN primary key(cod_fornecedor)
);

create table dependencia(
	cod_dep int not null,
    nome varchar(70) not null,
    cod_unidade int not null,
    constraint PK_DEP primary key(cod_dep),
    constraint FK_DEP_UBS foreign key dependencias(cod_unidade) references unidade(cod_unidade)
);

create table equipamento(
	cod_equip int not null,
    nome varchar(100) not null,
    data_recebido date not null,
    validade date not null,
    tipo varchar(20) not null,
    cod_fornecedor int not null,
    finalidade varchar(40) not null,
    cod_dep int,
    reutilizavel boolean not null,
	    
    constraint PK_EQUIP primary key(cod_equip),
	constraint FK_EQUIPAM foreign key equipamento(cod_dep) references dependencia(cod_dep),
    constraint FK_FORNEC foreign key equipamento(cod_fornecedor) references fornecedor(cod_fornecedor)
);



create table evento(
	id int,
    usuario int,
    evento varchar(60),
    data_hora datetime,
    msg varchar(200),
    
    constraint PK_LOG primary key evento(id),
    constraint FK_USR foreign key evento(usuario) references funcionario(cod_func)
);

drop table evento;


